extends Node2D

signal chapter_ended

enum State {
	FIRST_DIALOG,
	ROACH_GAME,
	SECOND_DIALOG,
	LASER_GAME,
	THIRD_DIALOG,
	BOMB_GAME,
	FINALE
}
export(int) var state = State.FIRST_DIALOG
export(int) var roaches_number = 20
export(int) var balls_number = 20
export var phrase_pause = 0.7

onready var Subtitles = $Subtitles
onready var Roaches = $Roaches
onready var Balls = $Balls
onready var SpeechPlayer = $SpeechPlayer
onready var Fish = $Fish
onready var Popup = $Popup
onready var Bomb = $Bomb
onready var Puzzle = $Puzzle
onready var GameOver = $GameOver
onready var Blop = get_node('/root/Game/SFX/Blop')
onready var Fade = get_node('Layer/Fade/Anim')
onready var FinBG = get_node('Finale/Background')
onready var FinWoman = get_node('Finale/Woman')
onready var FinAnim = get_node('Finale/AnimationPlayer')
onready var Thanks = get_node('Finale/Thanks')
onready var TSubtitles = get_node('Timers/Subtitles')
onready var TRoaches = get_node('Timers/Roaches')
# onready var TBalls = get_node('Timers/Balls')

onready var roach_scene = load('res://Roach.tscn')
# onready var ball_scene = load('res://Ball.tscn')

const first_monolog = [
	'hello', 'wondering', 'i_wonder', 'no_touchy', 'cant_stop', 'i_depend',
	'hungry', 'may_it_feed'
]
const second_monolog = [
	'really', 'assumed', 'just_fish', 'look_around'
]

const no_roach_monolog = [
	'no_more_roaches', 'relief', 'wont_repeat'
]

const bomb_first = [
	'try', 'everything_kills', 'no_use'
]

const bomb_second = [
	'hurry', 'youre_useless'
]

const bomb_third = [
	'screw_this', 'button_for_bomb', 'press_it', 'we_will_die'
]

func say(phrase_id, pause):
	Subtitles.set_text(SpeechPlayer.sub(phrase_id))
	SpeechPlayer.say(phrase_id)
	yield(SpeechPlayer, 'phrase_said')
	TSubtitles.wait_time = pause
	TSubtitles.start()

func clear_subs():
	Subtitles.set_text('')

func chapter_one():
	for x in first_monolog:
		match x:
			'i_depend': say(x, 1)
			_: say(x, phrase_pause)
		yield(TSubtitles, 'timeout')
		if x in ['wondering', 'cant_stop']:
			Fish.roll()
	clear_subs()
	# TODO sfx
	Popup.visible = true
	Fish.set_clickable(false)
	yield(Fish, 'button_hovered')
	Popup.visible = false

	# sets clickable to true on finish
	Fish.push_blender()
	for x in second_monolog:
		say(x, phrase_pause)
		yield(TSubtitles, 'timeout')

	var roach = first_roach()
	yield(roach, 'hovered')
	Fish.play_anim('scared')
	say('is_it_roach', phrase_pause)
	yield(TSubtitles, 'timeout')
	clear_subs()
	roach.staying = false
	emit_signal('chapter_ended')

func first_roach():
	var roach = roach_scene.instance()
	roach.position.x = 50
	roach.position.y = 500
	roach.staying = true
	Roaches.add_child(roach)
	return roach

func clear_roaches():
	for i in range(Roaches.get_child_count()):
		Roaches.get_child(i).queue_free()

func spawn_roaches():
	clear_subs()

	say('stop_roach', phrase_pause)
	var vp = get_viewport_rect().size
	for i in range(roaches_number):
		if i == 5:
			Fish.play_anim('fear')
			say('one_more_roach', phrase_pause)
		if i == 7: Fish.roll()
		if i == 10:
			Fish.play_anim('scared')
			say('stop_them', phrase_pause)
		if i == 13:
			Fish.play_anim('fear')
			say('im_scared', phrase_pause)
		if i == 15:
			Fish.play_anim('scared')
			say('there_are_more', phrase_pause)
		if i == 17: Fish.roll()
		var roach = roach_scene.instance()
		var x = randi() % 2
		var y = randi() % int(vp.y)
		roach.position.x = x*vp.x
		roach.position.y = y
		Roaches.add_child(roach)
		TRoaches.start()
		yield(TRoaches, 'timeout')
	emit_signal('chapter_ended')

func chapter_three():
	for x in no_roach_monolog:
		if x == 'no_more_roaches':
			Fish.roll()
		if x == 'relief':
			Fish.play_anim('phew')
		say(x, phrase_pause*2)
		yield(TSubtitles, 'timeout')

	clear_roaches()
	emit_signal('chapter_ended')

func bomb_game():
	clear_subs()

	Blop.play()
	Bomb.visible = true
	TSubtitles.wait_time = 1
	TSubtitles.start()
	yield(TSubtitles, 'timeout')
	Fish.roll()
	say('what_is_it', phrase_pause)
	yield(TSubtitles, 'timeout')

	Bomb.start()
	Puzzle.visible = true
	say('is_it_bomb', phrase_pause)
	Fish.play_anim('scared')
	yield(TSubtitles, 'timeout')

	for x in bomb_first:
		say(x, 2)
		match x:
			'is_it_bomb': Fish.play_anim('fear')
			'everything_kills': Fish.play_anim('angry')
			'no_use': Fish.play_anim('screw_it')
		yield(TSubtitles, 'timeout')

	TSubtitles.wait_time = 3
	TSubtitles.start()
	yield(TSubtitles, 'timeout')

	for x in bomb_second:
		say(x, 2)
		if x == 'hurry':
			Fish.roll()
		yield(TSubtitles, 'timeout')

	TSubtitles.wait_time = 3
	TSubtitles.start()
	yield(TSubtitles, 'timeout')

	for x in bomb_third:
		say(x, 2)
		match x:
			'screw_it': Fish.play_anim('screw_it')
			'we_will_die': Fish.play_anim('fear')
		yield(TSubtitles, 'timeout')

	Fish.play_anim('well_die')
	TSubtitles.wait_time = 2
	TSubtitles.start()
	yield(TSubtitles, 'timeout')

	emit_signal('chapter_ended')

func finale():
	clear_subs()
	Fade.play('out')
	yield(Fade, 'animation_finished')
	Bomb.visible = true
	FinBG.visible = true
	FinWoman.visible = true
	Bomb.peace()
	Fade.play('in')
	yield(Fade, 'animation_finished')
	FinAnim.play('catch')
	yield(FinAnim, 'animation_finished')
	TSubtitles.wait_time = 1
	TSubtitles.start()
	yield(TSubtitles, 'timeout')
	Fade.play('out')
	yield(Fade, 'animation_finished')
	Thanks.visible = true
	Bomb.visible = false
	Fade.play('in')
	yield(Fade, 'animation_finished')
	emit_signal('chapter_ended')

func plot():
	TSubtitles.wait_time = 1
	TSubtitles.start()
	yield(TSubtitles, 'timeout')

	chapter_one()
	state = State.FIRST_DIALOG
	yield(self, 'chapter_ended')

	spawn_roaches()
	state = State.ROACH_GAME
	yield(self, 'chapter_ended')

	chapter_three()
	state = State.THIRD_DIALOG
	yield(self, 'chapter_ended')

	bomb_game()
	state = State.BOMB_GAME
	yield(self, 'chapter_ended')

	finale()
	state = State.FINALE
	yield(self, 'chapter_ended')

func game_over():
	GameOver.visible = true
	get_tree().paused = true

func restart():
	GameOver.visible = false
	get_tree().paused = false
	SpeechPlayer.shut = false
	match state:
		State.FIRST_DIALOG:
			Fish.position.x = 512
			Fish.position.y = 260
		State.ROACH_GAME:
			clear_roaches()
		State.THIRD_DIALOG:
			clear_roaches()
		# State.BOMB_GAME:
			# Bomb.time = 50
			# Puzzle.reset()
	clear_subs()

func _ready():
	randomize()
	Fish.connect('button_pressed', self, 'game_over')
	get_node('GameOver/Button').connect('pressed', self, 'restart')
	plot()
	# to restart, call chapter function once again
