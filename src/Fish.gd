extends StaticBody2D

signal button_pressed
signal button_hovered

export var can_grab = false

var dead = false
var mouse_in = false
var grabbed = false
var grabbed_offset = Vector2()
var clickable = true

onready var Anim = $AnimationPlayer
onready var Button = $Button
onready var Shape = $Shape
onready var Fish = $Fish
onready var SpeechPlayer = get_node('/root/Game/SpeechPlayer')
onready var Clack = get_node('/root/Game/SFX/Clack')
onready var X_TRES = Shape.shape.radius
onready var Y_TRES = Shape.shape.height - 50

func _ready():
	randomize()
	Button.add_to_group('button')
	connect('mouse_entered', self, 'mouse_entered')
	connect('mouse_exited', self, 'mouse_exited')
	Button.connect('input_event', self, 'button_clicked')
	Button.connect('body_entered', self, 'body_entered')
	Button.connect('mouse_entered', self, 'hover_button')
	Anim.connect('animation_finished', self, 'animation_finished')
	Anim.play('idle')

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		grabbed = event.pressed
		grabbed_offset = position - get_global_mouse_position()

func _process(delta):
	if can_grab && grabbed && Input.is_mouse_button_pressed(BUTTON_LEFT):
		var new_pos = get_global_mouse_position() + grabbed_offset
		var vp = get_viewport_rect().size
		if new_pos.x < X_TRES: new_pos.x = X_TRES
		if new_pos.x > vp.x - X_TRES: new_pos.x = vp.x - X_TRES
		if new_pos.y < Y_TRES: new_pos.y = Y_TRES
		if new_pos.y > vp.y - Y_TRES: new_pos.y = vp.y - Y_TRES
		position = new_pos

func press_button():
	if !clickable: return
	Clack.play()
	dead = true
	Anim.play('ButtonPress')
	yield(Anim, 'animation_finished')
	Anim.play('flush')
	SpeechPlayer.say('flushed')
	SpeechPlayer.shut = true
	yield(Anim, 'animation_finished')
	emit_signal('button_pressed')

func roll():
	if Fish.scale.x == 1: Anim.play('roll')
	else: Anim.play('roll_back')

func play_anim(anim):
	if Anim.current_animation == 'flushed':
		return
	Anim.play(anim)

func set_clickable(value):
	clickable = value

func push_blender():
	Anim.play('push')

func hover_button():
	emit_signal('button_hovered')

func mouse_entered():
	mouse_in = true

func mouse_exited():
	mouse_in = false

func button_clicked(viewport, event, shape_idx):
	if event is InputEventMouseButton && event.pressed:
		press_button()

func body_entered(body):
	if body.is_in_group('roach'):
		press_button()

func animation_finished(name):
	if name == 'push':
		set_clickable(true)
	if name != 'idle':
		Anim.play('idle')
