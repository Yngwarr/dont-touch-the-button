extends Node

signal phrase_said

onready var phrases = {
	hello = [$Hello, "Hello, dear friend!"],
	wondering = [$Wondering, "You may be wondering why you are here and what is your goal."],
	i_wonder = [$IWonder, "Because I wonder why I am here and what this button does."],
	no_touchy = [$NoTouchy, "Oh, no! Don't press it right now! This thing looks like a blender!"],
	cant_stop = [$CantStop, "Well, obviously I can't stop you, because I'm just a fish."],
	i_depend = [$IDepend, "It turns out that my life depends on you."],
	hungry = [$Hungry, "Why don't you feed me? I'm hungry."],
	may_it_feed = [$MayItFeed, "Maybe this button can help?"],
	really = [$Really, "Wait, are you serious?"],
	assumed = [$Assumed, "I just *assumed* it may help feeding me."],
	just_fish = [$JustFish, "I'm just a fish, remember?"],
	is_it_roach = [$IsItRoach, "Oh gosh! What's this? Is it a roach?"],
	look_around = [$LookAround, "If you have nothing to do, look around."],
	stop_roach = [$StopRoach, "No! No-no, don't let it press the button! Stop it!"],
	one_more_roach = [$OneMoreRoach, "There! Here comes another one!"],
	stop_them = [$StopThem, "Stop them, p-please!"],
	im_scared = [$ImScared, "I'm scared!"],
	there_are_more = [$ThereAreMore, "There are more of them!"],
	no_more_roaches = [$NoMoreRoaches, "I think... I think that's it."],
	relief = [$Relief, "No more roaches?.. Everything's okay?.. Phew!"],
	wont_repeat = [$WontRepeat, "I hope this won't happen again."],
	what_is_it = [$WhatIsIt, "And what's this? Where did it come from?"],
	is_it_bomb = [$IsItBomb, "IS IT A BOMB? Seems like a- can you turn it off?"],
	try = [$Try, "Can you try at least?"],
	everything_kills = [$EverythingKills, "Why is everything trying to kill me?"],
	no_use = [$NoUse, "It's not working out."],
	hurry = [$Hurry, "Time's running out!"],
	youre_useless = [$YoureUseless, "It's weird, I am the fish, but you are the useless one."],
	screw_this = [$ScrewThis, "Agh, screw it! Press the button!"],
	button_for_bomb = [$ButtonForBomb, "Maybe this button will turn off the bomb?"],
	press_it = [$PressIt, "Press it! At least we'll find out what it does."],
	we_will_die = [$WeWillDie, "We will die. WE WILL DIE!!!"],
	flushed = [$Flushed, "A-a-a-a-ah, I'm being flu-u-u-ushed..."]
}

func _ready():
	for x in get_children():
		x.volume_db = 3

var shut = false

func say(id):
	if shut: return
	for x in phrases:
		phrases[x][0].stop()
	phrases[id][0].play()
	yield(phrases[id][0], 'finished')
	emit_signal('phrase_said')

func sub(id):
	return phrases[id][1]
