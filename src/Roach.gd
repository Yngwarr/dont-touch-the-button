extends KinematicBody2D

signal hovered

export (int) var speed = 100
export var turn_time_min = 6.0
export var turn_time_diff = 2.0
export var sound_time = 1.0

var velocity = Vector2()
var dead = false
var staying = false

onready var Button = get_node('/root/Game/Fish/Button')
onready var Sprite = $Sprite
onready var TimerNoize = get_node('Timers/Noize')
onready var Anim = $AnimationPlayer
onready var NoizeAnim = $NoizeAnimation
onready var Splash = get_node('/root/Game/SFX/Roach')

func _ready():
	connect('mouse_entered', self, 'mouse_entered')
	self.add_to_group('roach')
	$AnimationPlayer.play('run')
	turn()

	randomize()
	TimerNoize.wait_time = sound_time
	TimerNoize.connect('timeout', self, 'make_noize')
	TimerNoize.start()

func _physics_process(delta):
	if out_of_bounds():
		queue_free()
	if staying || dead: return
	Sprite.scale.y = 1.0 if abs(rotation) < PI/2 else -1.0
	velocity = Vector2(speed, 0).rotated(rotation)
	velocity = move_and_slide(velocity)

func _input_event(viewport, event, shape_idx):
	if dead: return
	if event is InputEventMouseButton:
		dead = true
		Splash.play()
		$Noize.visible = false
		NoizeAnim.stop()
		TimerNoize.stop()
		Anim.play('die')

func mouse_entered():
	emit_signal('hovered')

func out_of_bounds():
	var vp = get_viewport_rect().size
	return position.x < 0 || position.x > vp.x || position.y < 0 || position.y > vp.y

func turn():
	look_at(Button.global_position)

func make_noize():
	# TODO actual noize
	NoizeAnim.play('blink')
