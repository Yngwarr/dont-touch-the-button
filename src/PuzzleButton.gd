extends Area2D

signal toggled

export var on = false

onready var Anim = $AnimationPlayer
onready var Clack = get_node('/root/Game/SFX/Clack')

func _ready():
	turn(on)

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			toggle(true)
			Clack.play()

func turn(state):
	on = state
	Anim.play('on' if on else 'off')

func toggle(first):
	turn(!on)
	if first:
		emit_signal('toggled', false)
