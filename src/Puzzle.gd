extends Node2D

export var margin = 72

const conns = [
	[1,5,6],
	[0,2,5],
	[1,3,7],
	[2,7,4],
	[3,9],
	[0,6,10],
	[0,1,5,7],
	[8,11,12],
	[2,3,4],
	[4,13,14],
	[6,11,16],
	[5,10,12,15],
	[8,13,17],
	[8,9,14,18],
	[8,9,19],
	[10,20,21],
	[10,15,20,11],
	[12,22,23],
	[17,13,23,24],
	[18,14,24],
	[15,16,21],
	[20,16,17,22],
	[21,17,23],
	[22,24,19],
	[23,18,19]
]

const pressed = [0,1,4,5,10,11,14,15,18,19,21,22,23]

func _ready():
	var cs = get_children()
	for i in range(5):
		for j in range(5):
			var c = cs[j*5+i]
			c.position.x = i*margin
			c.position.y = j*margin
	for i in range(len(conns)):
		for j in conns[i]:
			cs[i].connect('toggled', cs[j], 'toggle')
	reset()

func reset():
	var cs = get_children()
	for i in range(len(cs)):
		cs[i].turn(i in pressed)
