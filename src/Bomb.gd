extends Node2D

signal out_of_time

onready var Timer = $Timer
onready var Label = $Time
onready var Anim = $AnimationPlayer
onready var Sprite = $Sprite

var time = 50
var off = false

func _ready():
	Timer.connect('timeout', self, 'decrement')
	Label.text = '0:%s' % time

func decrement():
	if off: return
	time -= 1
	Label.text = ('0:%s' if time >= 10 else '0:0%s') % time
	if time == 0:
		off = true
		emit_signal('out_of_time')
		Anim.play('blink')
		# TODO sfx

func start():
	Timer.start()

func peace():
	Sprite.frame = 0
	Anim.stop()
	Label.text = '1:13'
